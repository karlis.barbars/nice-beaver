<?php
use App\Core\Application;

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $this->title ?></title>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.9.3/css/bulma.min.css">
    <script src="https://kit.fontawesome.com/e83878c67a.js" crossorigin="anonymous"></script>
</head>
<body>
    <!-- NAVIGATION BAR -->
    <nav class="navbar is-transparent is-spaced">
        <div class="container">
            <!-- NAV RIGHT -->

                <?php if(Application::isGuest()): ?>
                <div class="navbar-start">
                    <div class="notification is-warning">
                        You have to login to use the application.
                    </div>
                </div>
                 <div class="navbar-end">
                    <a href="/login" class="navbar-item">
                        <button class="button is-success is-light is-outlined is-responsive">
                            <span><b>Login</b></span>
                            <span class="icon is-small"><i class="fa-solid fa-arrow-right"></i></span>
                        </button>
                    </a>
                    <a href="/register" class="navbar-item">
                        <button class="button is-warning is-light is-outlined is-responsive">
                            <span><b>Register</b></span>
                            <span class="icon is-small"><i class="fa-solid fa-arrow-right"></i></span>
                        </button>
                    </a>
                     <?php if(Application::$app->session->getFlash('success')): ?>
                         <div class="notification is-success">
                             <?php echo Application::$app->session->getFlash('success')?>
                         </div>
                     <?php endif; ?>
                <?php else: ?>
                    <!-- NAV LEFT -->
                    <div class="navbar-start">
                        <a href="/" class="navbar-item">
                            <button class="button is-light is-responsive">
                                <span class="icon is-small"><i class="fa-solid fa-house"></i></span>
                                <span><b>Home</b></span>
                            </button>
                        </a>
                        <a href="/gps" class="navbar-item">
                            <button class="button is-light is-responsive">
                                <span class="icon is-small"><i class="fa-solid fa-location-dot"></i></span>
                                <span><b>Map</b></span>
                            </button>
                        </a>
                    </div>
                     <div class="container navbar-item" style="max-width: 40em">
                    <?php if(Application::$app->session->getFlash('success')): ?>
                        <div class="notification is-success navbar-item">
                            <?php echo Application::$app->session->getFlash('success')?>
                        </div>
                    <?php endif; ?>
                     </div>
                    <!-- END OF NAV LEFT -->
                    <div class="navbar-end">
                        <p class="navbar-item"><span class="tag is-medium is-responsive  is-light" style="padding:20px">Greetings, <span style="font-weight: bold; margin-left: 0.2em"> <?php echo Application::$app->user->getDisplayName()?> </span>!</span></p>

                        <a href="/logout" class="navbar-item">
                            <button class="button is-danger is-light is-outlined is-responsive">
                                <span><b>Logout</b></span>
                                <span class="icon is-small"><i class="fa-solid fa-arrow-right"></i></span>
                            </button>
                        </a>
                    </div>
                <?php endif; ?>


            </div>
            <!-- END OF NAV RIGHT -->
        </div>
    </nav>
    <!-- END OF NAVIGATION BAR -->

    <div class="container" style="max-width: 40em">

    </div>

    <div class="container">
        {{content}}
    </div>

</body>
</html>