<?php
use App\Core\form\Form;
/** @var $model App\Models\User */
?>
<section class="section is-large">
    <div class="container box is-max-desktop" style="max-width: 30em">
        <label class="label">Login</label>

        <?php $form = Form::begin('', "post") ?>
        <?php  echo $form->field($model, 'email') ?>
        <?php  echo $form->field($model, 'password')->passwordField() ?>


        <button class="button is-success" type="submit" >Login</button>
        <a href="/register" ><button class="button is-light mx-2" type="button">Don't have an account? Sign up!</button></a>
        <?php echo Form::end() ?>
    </div>
</section>