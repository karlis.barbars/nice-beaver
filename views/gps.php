<?php
/** @var $this View */
/** @var $model RouteForm */

use App\Core\form\Form;
use App\Core\View;
use App\Models\RouteForm;

$this->title = 'GPS';
?>
<script>
    function initMap() {
        const center = new google.maps.LatLng(56.9349748, 24.1389312);
        map = new google.maps.Map(document.getElementById("map"), {
            zoom: 5,
            center: center,
            mapId: 'f0ac9fd72e8da676',
            disableDefaultUI: true,
        });
    }
</script>

<div class="container">
    <div class="columns">
        <!-- LEFT COLUMN -->
        <div class="column">
            <!-- SELECT VEHICLE BOX -->
            <div  class="box" >
                <!-- SEARCH BAR -->
                <div class="block">
                    <p class="control has-icons-left">
                        <input class="input" type="text" placeholder="Search" disabled>
                        <span class="icon is-left"><i class="fas fa-search" aria-hidden="true"></i></span>
                    </p>
                </div>
                <!-- SELECT VEHICLE -->
                <div class="select-vehicle">

                        <a id="select-vehicle" class="panel-block is-size-7 is-family-primary button is-white" >
                            <span class="panel-icon has-text-grey-lighter "><i class="fa-solid fa-car"></i></span>
                        </a>
                </div>
            </div>
            <!-- END OF SELECT VEHICLE -->

            <!-- SELECT DATE AND TIME -->
            <?php $form = \App\Core\form\Form::begin('', 'post') ?>
                <div  class="box">
                    <div class="block">
                        <div class="field is-horizontal">
                            <div class="field-body">
                                <div class="field has-addons">
                                    <p class="control"><a class="button is-static is-small">From</a></p>
                                    <p class="control">
                                        <?php echo $form->field($model,'from')->dateTime() ?>
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="field is-horizontal">
                            <div class="field-body">
                                <div class="field has-addons">
                                    <p class="control"><a class="button is-static is-small">Till</a></p>
                                    <p class="control">
                                        <?php echo $form->field($model,'till')->dateTime() ?>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="block">
                    <p class="buttons is-centered">
                        <button class="button is-success is-small" type="submit" name="show-route">
                            <span class="icon"><i class="fa-solid fa-route"></i></span>
                            <span>Route</span>
                        </button>

                        <button class="button is-light is-small" type="reset" value="Clear">
                            <span>Clear</span>
                            <span class="icon is-small"><i class="fas fa-times"></i></span>
                        </button>
                    </p>
                </div>
            <?php \App\Core\form\Form::end() ?>
            <!-- END OF SELECT DATE TIME -->
        </div>
        <!-- END OF LEFT COLUMN -->


        <!-- RIGHT COLUMN -->
        <div class="column is-four-fifths">
            <!-- GOOGLE MAPS-->
            <div id="map" class="box is-fullhd" style="height:85vh"></div>
        </div>
        <!-- END OF RIGHT COLUMN-->
    </div>
</div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>



<!-- Async script executes immediately and must be after any DOM elements used in callback. -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBp3yOkpQenWUUTfrD-YehKQySbgw_vbl4&callback=initMap" async></script>

