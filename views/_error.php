<?php
/** @var $exception \Exception */
?>
    <div class='container is-fluid' style="padding-top: 5em">
        <div class='notification has-text-centered is-light container box' style='width: 30em; height: 20em;'>
        <i class='fa-solid fa-triangle-exclamation block' style='font-size:5em; padding-top: 0.3em;'></i>
            <p class='title is-1'><?php echo $exception->getCode()?></p>
            <p class='subtitle is-4'><?php echo $exception->getMessage()?></p>
        </div>
    </div>
