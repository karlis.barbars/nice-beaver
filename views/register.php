<?php
use App\Core\form\Form;
/** @var $model App\Models\User */

?>
<section class="section is-large">
    <div class="container box is-max-desktop" style="max-width: 30em">
        <label class="label">Sign Up</label>

        <?php $form = Form::begin('', "post") ?>
            <?php  echo $form->field($model, 'username') ?>
            <?php  echo $form->field($model, 'password')->passwordField() ?>
            <?php  echo $form->field($model, 'confirmPassword')->passwordField() ?>
            <?php  echo $form->field($model, 'email') ?>

        <button class="button is-success" type="submit" >Sign up</button>
        <a href="/login" ><button class="button is-light mx-2" type="button">Already have an account?</button></a>
        <?php echo Form::end() ?>
    </div>
</section>