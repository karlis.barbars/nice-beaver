# Mapon Task
Back-end task.
extended.

## Deploy application

1. git clone this repository
2. cd into docker directory
```cd docker```
3. run ```docker-compose build```
4. run ```docker-compose up -d```
5. ssh into the app container ``` docker exec -it entrytask-app bash```
6. run ```composer install```
7. ```cd docker```
8. ssh into the db container ```docker exec -it entrytask-db bash```
9. ```mysql -u root -p``` and enter your password
10. ```CREATE DATABASE entrytask```

11. Either run migrations.php file or in public/index.php uncomment migrations function.
