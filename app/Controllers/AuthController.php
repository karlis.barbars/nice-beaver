<?php
namespace App\Controllers;

use App\Core\Application;
use App\Core\Controller;
use App\Core\Middlewares\AuthMiddleware;
use App\Core\Request;
use App\Core\Response;
use App\Models\LoginForm;
use App\Models\RouteForm;
use App\Models\User;

class AuthController extends Controller
{
    public function __construct()
    {
        // Array of routes that need authentication.
        $this->registerMiddleware(new AuthMiddleware(['gps']));
    }

    // Home page.
    public function home()
    {
        $params = [
            'name' => ''
        ];
        return $this->render('home', $params);
    }

    public function login(Request $request, Response $response)
    {
        // Create a new login form
        $loginForm = new LoginForm();
        if($request->isPost()) {
            $loginForm->loadData($request->getBody());
            if($loginForm->validate() && $loginForm->login()) {
                // After successful login, redirect.
                $response->redirect('/gps');
                return;
            }
        }
        // set layout (navbar) to auth and render the login form.
        $this->setLayout('auth');
        return $this->render('login', [
            'model' => $loginForm
        ]);
    }

    public function register(Request $request)
    {
        // Create new user model.
        $user = new User();
        if($request->isPost()) {
            $user->loadData($request->getBody());

            // Validate if user input is clean.
            if($user->validate() && $user->save()){
                // Save user to database and redirect with a flash message.
                Application::$app->session->setFlash('success', 'Your account has been created. You can now login!');
                Application::$app->response->redirect('/');
                exit;
            }
            // Set layout (navbar) to auth and render the register form.
            $this->setLayout('auth');
            return $this->render('register', [
                'model' => $user
            ]);
        }
        // Set layout (navbar) to auth and render the register form.
        $this->setLayout('auth');
        return $this->render('register', [
            'model' => $user
        ]);
    }

    public function logout(Request $request, Response $response)
    {
        // Logout and redirect.
        Application::$app->logout();
        $response->redirect('/');

    }

    public function gps(Request $request, Response $response)
    {
        // Create new Route Form (select date and time)
        $route = new RouteForm();
        if ($request->isPost()) {
            // If theres a request, get request body.
            $route->loadData($request->getBody());
            if ($route->validate() && $route->send()) {
                // Send request, show flash message and redirect.
                Application::$app->session->setFlash('success', 'Finding routes...');
                return $response->redirect('/gps');
            }
        }
        // render view gps, pass route model.
        return $this->render('gps', [
            'model' => $route
        ]);
    }
}