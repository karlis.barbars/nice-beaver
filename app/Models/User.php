<?php

namespace App\Models;
use App\Core\DatabaseModel;
use App\Core\UserModel;


class User extends UserModel
{
    public string $username = '';
    public string $email = '';
    public string $password = '';
    public string $confirmPassword = '';

    public static function tableName(): string
    {
        // Define users table.
        return 'users';
    }

    public static function primaryKey(): string
    {
        // Define primary key row.
        return 'id';
    }

    public function save()
    {
        $this->password = password_hash($this->password, PASSWORD_DEFAULT);
        return parent::save();
    }

    public function rules(): array
    {
        // Set rules for register fields.
        return [
            'username' => [self::RULE_REQUIRED],
            'password' => [self::RULE_REQUIRED, [self::RULE_MIN, 'min' => 8], [self::RULE_MAX, 'max' => 48]],
            'confirmPassword' => [self::RULE_REQUIRED, [self::RULE_MATCH, 'match' => 'password']],
            'email' => [self::RULE_REQUIRED, self::RULE_EMAIL, [self::RULE_UNIQUE, 'class' => self::class]],
        ];
    }

    public function attributes(): array
    {
        // set Attributes
        return ['username', 'email', 'password'];
    }

    public function labels(): array
    {
        // Set labels for register fields.
        return [
            'username' => 'Username',
            'password' => 'Password',
            'confirmPassword' => 'Confirm password',
            'email' => 'Email',
        ];
    }

    public function icons(): array
    {
        // Set icons for register fields.
        return [
            'username' => 'fas fa-user',
            'password' => 'fas fa-lock',
            'confirmPassword' => 'fa-solid fa-key',
            'email' => 'fa-solid fa-envelope',
        ];
    }

    public function getDisplayName(): string
    {
        // Get users username.
        return $this->username;
    }

}