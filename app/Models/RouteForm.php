<?php

namespace App\Models;

use App\Core\Model;

class RouteForm extends Model
{
    public string $from = '';
    public string $till = '';

    public function rules(): array
    {
        // Set rules for date-time field.
        return [
            'from' => [self::RULE_REQUIRED],
            'till' => [self::RULE_REQUIRED]
        ];
    }

    public function labels(): array
    {
        // Set labels for date-time field.
        return [
            'from' => 'From',
            'till' => 'Till'
        ];
    }

    public function icons(): array
    {
        return [];
    }

    public function send() // didnt have time to finish :(
    {
        return true;
    }
}