<?php

namespace App\Models;
use App\Core\Application;
use App\Core\Model;

class LoginForm extends Model
{
    public string $email = '';
    public string $password = '';

    public function rules(): array
    {
        // Set rules for login form.
        return [
            'email' => [self::RULE_REQUIRED, self::RULE_EMAIL],
            'password' => [self::RULE_REQUIRED]
        ];
    }

    public function login()
    {
        // Find user in database
        $user = (new User)->findOne(['email' => $this->email]);
        if(!$user) { // Validate if user exists
            $this->addError('email', 'User does not exist with this email!');
            return false;
        }
        // Validate password.
        if(!password_verify($this->password, $user->password)) {
            $this->addError('password', 'Password is incorrect!');
            return false;
        }

        // Login if successful.
        return Application::$app->login($user);
    }

    public function labels(): array
    {
        // Labels for fields.
        return [
            'email' => 'Email',
            'password' => 'Password',

        ];
    }

    public function icons(): array
    {
        // Icons for fields.
        return [
            'email' => 'fa-solid fa-envelope',
            'password' => 'fas fa-lock',

        ];
    }
}