<?php

namespace App\Core;

class Application
{
    // Set application root directory
    public static string $ROOT_DIR;

    // Default layout
    public string $layout = 'main';
    public string $userClass;

    public Router $router;
    public Request $request;
    public Response $response;
    public Session $session;
    public Database $db;
    public ?DatabaseModel $user;
    public View $view;

    public static Application $app;
    public ?Controller $controller = null;
    public function __construct($rootPath, array $config)
    {
        // Construct application
        self::$ROOT_DIR = $rootPath;
        self::$app = $this;
        $this->request = new Request();
        $this->response = new Response();
        $this->session = new Session();
        $this->router = new Router($this->request, $this->response);
        $this->view = new View();

        // Get config from public/index.php
        $this->db = new Database($config['db']);
        $this->userClass = $config['userClass'];

        // Get user from Session
        $primaryValue = $this->session->get('user');
        if($primaryValue) {
            $primaryKey = $this->userClass::primaryKey(); // Returns id
            $this->user = $this->userClass::findOne([$primaryKey => $primaryValue]); // Returns user.
        } else {
            $this->user = null;
        }
    }

    public static function isGuest()
    {
        // Check if user is guest.
        return !self::$app->user;
    }

    public function run()
    {
        try { // Try start application
            echo $this->router->resolve();
        } catch(\Exception $e) {
            $this->response->setStatusCode($e->getCode()); // Set status code.
            echo $this->view->renderView('_error', [
                'exception' => $e
            ]);
        }
    }

    public function login(DatabaseModel $user)
    {
        // Login user, set session.
        $this->user = $user;
        $primaryKey = $user->primaryKey();
        $primaryValue = $user->{$primaryKey};
        $this->session->set('user', $primaryValue);
        return true;
    }

    public function logout()
    {
        // Logout user.
        $this->user = null;
        $this->session->remove('user');
    }
}