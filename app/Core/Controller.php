<?php

namespace App\Core;

use App\Core\Middlewares\BaseMiddleware;

class Controller
{
    // set default layout.
    public string $layout = 'main';
    public string $action = '';

    /**
     * @var \App\Core\Middlewares\BaseMiddleware[]
     */
    protected array $middlewares = [];

    public function setLayout($layout)
    {
        // Set Layout.
        $this->layout = $layout;
    }

    public function render($view, $params = [])
    {
        // Render View.
        return Application::$app->view->renderView($view, $params);
    }

    public function registerMiddleware(BaseMiddleware $middleware)
    {
        // Register middleware.
        $this->middlewares[] =  $middleware;
    }

    /**
     * @return Middlewares\BaseMiddleware[]
     */
    public function getMiddlewares(): array
    {
        // Return middleware.
        return $this->middlewares;
    }
}