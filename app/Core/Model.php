<?php

namespace App\Core;

abstract class Model
{

    // Define required rules used to validate data.
    public const RULE_REQUIRED = 'required';
    public const RULE_EMAIL = 'email';
    public const RULE_MIN = 'min';
    public const RULE_MAX = 'max';
    public const RULE_MATCH = 'match';
    public const RULE_UNIQUE = 'unique';

    public function loadData($data)
    {
        foreach($data as $key => $value) {
            if(property_exists($this, $key)) {
                $this->{$key} = $value;
            }
        }
    }

    abstract public function rules(): array;
    abstract public function labels(): array;
    abstract public function icons(): array;

    public function getLabel($attribute)
    {
        // Return label.
        return $this->labels()[$attribute] ?? $attribute;
    }

    public array $errors = [];

    public function validate()
    {
        // Validate input data.
        foreach($this->rules() as $attribute => $rules) {
            $value = $this->{$attribute};
            foreach($rules as $rule) {
                $ruleName = $rule;

                if(!is_string($ruleName)) {
                    $ruleName = $rule[0];
                }
                if($ruleName === self::RULE_REQUIRED && !$value) {
                    $this->addErrorForRule($attribute, self::RULE_REQUIRED);
                }
                if($ruleName === self::RULE_EMAIL && !filter_var($value, FILTER_VALIDATE_EMAIL)) {
                    $this->addErrorForRule($attribute, self::RULE_EMAIL);
                }
                if($ruleName === self::RULE_MIN && strlen($value) < $rule['min']) {
                    $this->addErrorForRule($attribute, self::RULE_MIN, $rule);
                }
                if($ruleName === self::RULE_MAX && strlen($value) > $rule['max']) {
                    $this->addErrorForRule($attribute, self::RULE_MAX, $rule);
                }
                if($ruleName === self::RULE_MATCH && $value !== $this->{$rule['match']}) {
                    $rule['match'] = $this->getLabel($rule['match']);
                    $this->addErrorForRule($attribute, self::RULE_MATCH, $rule);
                }
                // Check if data is unique
                if($ruleName === self::RULE_UNIQUE) {
                    $className = $rule['class'];
                    $uniqueAttr = $rule['attribute'] ?? $attribute;
                    $tableName = $className::tableName();
                    $statement = Application::$app->db->prepare("SELECT * FROM $tableName WHERE $uniqueAttr = :attr");
                    $statement->bindValue(":attr", $value);
                    $statement->execute();
                    $record = $statement->fetchObject();

                    if($record) {
                        $this->addErrorForRule($attribute, self::RULE_UNIQUE, ['field' => $this->getLabel($attribute)]);
                    }
                }
            }
        }

        return empty($this->errors);
    }


    private function addErrorForRule(string $attribute, string $rule, $params = [])
    {
        // Add error messages if validation fails.
        $message = $this->errorMessages()[$rule] ?? '';
        foreach($params as $key => $value) {
            $message = str_replace("{{$key}}", $value, $message);
        }
        $this->errors[$attribute][] = $message;
    }

    public function addError(string $attribute, string $message)
    {
        // Add error.
        $this->errors[$attribute][] = $message;
    }

    public function errorMessages()
    {
        // Define error messages for validation errors.
        return [
            self::RULE_REQUIRED => 'This field is required!',
            self::RULE_EMAIL => 'This field must be valid email address!',
            self::RULE_MIN => 'This field must be at least {min} characters long!',
            self::RULE_MAX => 'Password can not be greater than {max} characters!',
            self::RULE_MATCH => 'This field must be the same as {match}!',
            self::RULE_UNIQUE => 'This {field} is already taken!',
        ];
    }

    public function hasError($attribute)
    {
        // Check if attribute has an error.
        return $this->errors[$attribute] ?? false;
    }

    public function getFirstError($attribute)
    {
        // Get first error on attribute.
        return $this->errors[$attribute][0] ?? false;
    }
}