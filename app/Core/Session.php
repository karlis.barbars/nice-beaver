<?php

namespace App\Core;

class Session
{
    protected const FLASH_KEY = 'flash_messages';
    public function __construct()
    {
        // Start session, display flash messages if they exist.
        session_start();
        $flashMessages = $_SESSION[self::FLASH_KEY] ?? [];
        foreach($flashMessages as $key => &$flashMessage) {
            $flashMessage['remove'] =  true;
        }

        $_SESSION[self::FLASH_KEY] = $flashMessages;
    }

    public function setFlash($key, $message)
    {   // Set flash message.
        $_SESSION[self::FLASH_KEY][$key] = [
            'remove' => false,
            'value' => $message,
        ];
    }

    public function getFlash($key)
    {   // get flash message
        return $_SESSION[self::FLASH_KEY][$key]['value'] ?? false;
    }

    public function set($key, $value)
    {   // Set session
        $_SESSION[$key] = $value;
    }

    public function get($key)
    {   // Get session
        return $_SESSION[$key] ?? false;
    }

    public function remove($key)
    {   // Remove session
        unset($_SESSION[$key]);
    }

    public function __destruct()
    {   // Destroy flash messages on reload.
        $flashMessages = $_SESSION[self::FLASH_KEY] ?? [];
        foreach($flashMessages as $key => &$flashMessage) {
            if($flashMessage['remove']) {
                unset($flashMessages[$key]);
            }
        }
        $_SESSION[self::FLASH_KEY] = $flashMessages;
    }
}