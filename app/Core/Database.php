<?php
namespace App\Core;

class Database
{
    public \PDO $pdo;
    public function __construct(array $config)
    {
        // Get data from .env file.
        $dsn = $config['dsn'] ?? '';
        $user = $config['user'] ?? '';
        $password = $config['password'] ?? '';
        $this->pdo = new \PDO($dsn, $user, $password);
        $this->pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
    }

    public function applyMigrations()
    {
        // Create migrations table in database.
        $this->createMigrationsTable();
        $appliedMigrations = $this->getAppliedMigrations();

        $newMigrations = [];
        // Scans migrations folder for any new migrations.
        $files = scandir(Application::$ROOT_DIR.'/migrations');
        $toAppliedMigrations = array_diff($files, $appliedMigrations);

        // Foreach migration in directory, apply directory.
        foreach($toAppliedMigrations as $migration) {
            if($migration === '.' || $migration === '..') {
                continue;
            }

            require_once Application::$ROOT_DIR.'/migrations/'.$migration;
            $className = pathinfo($migration, PATHINFO_FILENAME);
            $instance = new $className();
            $this->log("Applying Migration $migration");
            $instance->up();
            $this->log("Applied Migration $migration");
            $newMigrations[] = $migration;
        }
        if(!empty($newMigrations)) {
            $this->saveMigrations($newMigrations);
        } else {
            $this->log("All migrations applied!");
        }
    }

    public function createMigrationsTable()
    {
        // Create migrations table.
        $this->pdo->exec("CREATE TABLE IF NOT EXISTS migrations (
            id INT AUTO_INCREMENT PRIMARY KEY, 
            migration VARCHAR(255),
            created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
            ) ENGINE=INNODB;");
    }

    public function getAppliedMigrations()
    {
        // From database check which migrations have been already applied.
        $statement =  $this->pdo->prepare("SELECT migration FROM migrations");
        $statement->execute();

        return $statement->fetchAll(\PDO::FETCH_COLUMN);
    }


    public function saveMigrations(array $migrations)
    {
        // Save migrations to database.
        $str = implode(",", array_map(fn($m) => "('$m')", $migrations));

        $statement = $this->pdo->prepare("INSERT INTO migrations (migration) VALUES $str");
        $statement->execute();
    }

    public function prepare($sql) {
        return $this->pdo->prepare($sql);
    }

    protected function log($message)
    {
        // Log to terminal or display on webpage migration info.
        echo '['.date('Y-m-d H:i:s').'] - '.$message.PHP_EOL;
    }
}