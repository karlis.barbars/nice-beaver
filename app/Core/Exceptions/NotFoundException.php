<?php

namespace App\Core\Exceptions;

class NotFoundException extends \Exception
{
    protected $message = 'Page does not exist.';
    protected $code = 404;

}