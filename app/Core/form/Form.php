<?php
namespace App\Core\form;

use App\Core\Model;

class Form
{
    public static function begin($action, $method)
    {
        // create Form.
        echo sprintf('<form action="%s" method="%s">', $action, $method);
        return new Form();
    }

    public static function end()
    {
        // end Form.
        echo'</form>';
    }

    public function field(Model $model, $attribute)
    {
        return new Field($model, $attribute);
    }
}