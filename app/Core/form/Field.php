<?php

namespace App\Core\form;

use App\Core\Model;

class Field
{
    // Create constants for validation.
    public const TYPE_TEXT = 'text';
    public const TYPE_PASSWORD = 'password';
    public const TYPE_NUMBER = 'number';
    public const TYPE_DATETIME = 'datetime-local';

    public string $type;
    public Model $model;
    public string $attribute;

    /**
     * @param Model $model
     * @param string $attribute
     */
    public function __construct(Model $model, string $attribute)
    {
        // Construct field
        $this->type = self::TYPE_TEXT;
        $this->model = $model;
        $this->attribute = $attribute;
    }

    public function __toString(): string
    {
        // Return field.
        return sprintf('            
            <div class="field">
                <div class="control has-icons-left has-icons-right">
                    <input type="%s" placeholder="%s" name="%s"  class="input is-small%s">
                    <span class="icon is-small is-left"><i class="%s"></i></span>
                    <p class="help is-danger">%s</p>
                </div>
            </div>',
                $this->type, // Type of field.
                $this->model->getLabel($this->attribute), // Placeholder or label
                $this->attribute, // Name

                $this->model->hasError($this->attribute) ? ' is-danger' : '', // Classes
                $this->model->icons()[$this->attribute] ?? '', // Icons
                $this->model->getFirstError($this->attribute) // Error messages
        );
    }

    public function passwordField()
    {
        // Set field type to password.
        $this->type = self::TYPE_PASSWORD;
        return $this;
    }

    public function dateTime()
    {
        // Set field type to date-time.
        $this->type = self::TYPE_DATETIME;
        return $this;
    }
}